package middleware

import (
	"fmt"
	"os"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v5"
	"gitlab.com/Black-Sirius69/blog/initializers"
	"gitlab.com/Black-Sirius69/blog/models"
)

func AuthUser(c *fiber.Ctx) error {
	tokenString := c.Cookies("Auth", "nil")

	if tokenString == "nil" {
		return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{"message": "Failed to authorize"})
	}

	token, _ := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected Signing Method")
		}
		return []byte(os.Getenv("SECRET")), nil
	})

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		if float64(time.Now().Unix()) > claims["exp"].(float64) {
			return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{"message": "JWT Expired"})
		}

		var user models.User
		initializers.Db.Find(&user, "user_name = ?", claims["sub"])

		if user.ID == 0 {
			c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{"message": "Invalid User"})
		}

		c.Locals("user", user)

		return c.Next()
	} else {
		return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{"message": "Internal error"})
	}
}
