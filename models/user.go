package models

import "gorm.io/gorm"

type User struct {
	gorm.Model
	FirstName string `json:"firstName"`
	LastName string `json:"lastName"`
	UserName string `gorm:"unique;primaryKey" json:"userName"`
	Email string `gorm:"unique" json:"email"`
	Password string	`json:"password"`
	Posts []Post `gorm:"foreignKey:Writer;References:UserName;constraint:OnUpdate:CASCADE,OnDelete:CASCADE"`
}

