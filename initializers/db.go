package initializers

import (
	"log"
	"os"

	"gitlab.com/Black-Sirius69/blog/models"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var Db *gorm.DB

func ConnectDB() {
	var err error
	dsn := os.Getenv("DB_URL")
	Db, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal("Failed to connect to database")
	}
}

func SyncDB() {
	Db.AutoMigrate(&models.User{}, &models.Post{})
}
