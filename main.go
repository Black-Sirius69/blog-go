package main

import (
	"os"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/Black-Sirius69/blog/initializers"
	"gitlab.com/Black-Sirius69/blog/middleware"
	"gitlab.com/Black-Sirius69/blog/routes"
)

func init() {
	initializers.LoadEnvVars()
	initializers.ConnectDB()
	initializers.SyncDB()
}

func main() {
	app := fiber.New()
	app.Get("/", func(c *fiber.Ctx) error {
		return c.SendString("Hello World")
	})

	app.Post("/api/users", routes.CreateUser)
	app.Put("/api/users/:name", routes.UpdateUser)
	app.Delete("/api/users/:name", routes.DeleteUser)
	app.Get("/api/users/:name", routes.GetUser)
	app.Get("/login", routes.Login)

	app.Post("/api/articles", middleware.AuthUser, routes.CreatePost)
	app.Get("/api/articles/search", routes.GetPosts)
	app.Delete("/api/articles/delete/:id", routes.DeletePost)

	app.Listen(":" + os.Getenv("PORT"))
}
