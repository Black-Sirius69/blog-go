package routes

import (
	"os"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v5"
	"gitlab.com/Black-Sirius69/blog/initializers"
	"gitlab.com/Black-Sirius69/blog/models"
	"golang.org/x/crypto/bcrypt"
)

func CreateUser(c *fiber.Ctx) error {
	var user models.User

	if err := c.BodyParser(&user); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(err.Error())
	}

	hash, err := bcrypt.GenerateFromPassword([]byte(user.Password), 10)

	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"message": "Failed to generate hash"})
	}

	user.Password = string(hash)

	if result := initializers.Db.Create(&user); result.Error != nil {
		return c.Status(fiber.StatusBadRequest).JSON(result.Error.Error())
	}

	return c.Status(fiber.StatusOK).JSON(user)
}

func GetUser(c *fiber.Ctx) error {
	var user models.User
	userName := c.Params("name", "nil")

	if userName == "nil" {
		return c.Status(fiber.StatusBadRequest).SendString("Please enter a valid user name")
	}

	initializers.Db.Preload("Posts").Find(&user, "user_name = ?", userName)
	if user.ID == 0 {
		return c.Status(fiber.StatusBadRequest).SendString("User does not exist")
	}

	return c.Status(fiber.StatusOK).JSON(user)
}

func UpdateUser(c *fiber.Ctx) error {
	var user models.User

	userName := c.Params("name")

	initializers.Db.Find(&user, "user_name = ?", userName)
	if user.ID == 0 {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"message": "User does not exist"})
	}

	var Data struct {
		FirstName string `json:"firstName"`
		LastName  string `json:"lastName"`
		UserName  string `json:"userName"`
		Email     string `json:"email"`
		Password  string `json:"password"`
	}

	if err := c.BodyParser(&Data); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"message": "Pass the update data into body"})
	}

	if len(Data.FirstName) != 0 {
		user.FirstName = Data.FirstName
	}

	if len(Data.LastName) != 0 {
		user.LastName = Data.LastName
	}

	if len(Data.UserName) != 0 {
		user.UserName = Data.UserName
	}

	if len(Data.Email) != 0 {
		user.Email = Data.Email
	}

	if len(Data.Password) != 0 {
		hash, err := bcrypt.GenerateFromPassword([]byte(Data.Password), 10)
		if err != nil {
			return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"message": "Failed to hash password"})
		}

		user.Password = string(hash)
	}

	initializers.Db.Save(&user)
	return c.Status(fiber.StatusOK).JSON(fiber.Map{"message": "Successfully update data", "data": user})
}

func DeleteUser(c *fiber.Ctx) error {
	var user models.User

	userName := c.Params("name")

	initializers.Db.Find(&user, "user_name = ?", userName)
	if user.ID == 0 {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"message": "User not found"})
	}

	if err := initializers.Db.Unscoped().Delete(&user).Error; err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"message": "Failed to delete user"})
	}

	return c.Status(fiber.StatusOK).JSON(fiber.Map{"messsage": "Successfully Delete user"})
}

func Login(c *fiber.Ctx) error {
	var user models.User

	var body struct {
		Username string
		Email    string
		Password string
	}

	if err := c.BodyParser(&body); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"message": "Please pass the data"})
	}

	if len(body.Username) != 0 {
		initializers.Db.Find(&user, "user_name = ?", body.Username)
	} else {
		initializers.Db.Find(&user, "email = ?", body.Email)
	}

	if user.ID == 0 {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"message": "User does not exist"})
	}

	err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(body.Password))

	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"message": "Invalid password"})
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"sub": user.UserName,
		"exp": time.Now().Add(time.Hour * 24 * 30).Unix(),
	})

	tokenString, err := token.SignedString([]byte(os.Getenv("SECRET_KEY")))

	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"message": "Failed to create JWT"})
	}

	cookie := fiber.Cookie{
		Name:     "Auth",
		Value:    tokenString,
		Expires:  time.Now().Add(time.Hour * 24 * 30),
		HTTPOnly: true,
	}

	c.Cookie(&cookie)
	return c.Status(fiber.StatusOK).JSON(fiber.Map{"message": "Login Successfull"})

}
