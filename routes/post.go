package routes

import (
	"net/url"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/Black-Sirius69/blog/initializers"
	"gitlab.com/Black-Sirius69/blog/models"
)

func CreatePost(c *fiber.Ctx) error {
	var post models.Post

	var data struct {
		Title   string
		Content string
	}

	if err := c.BodyParser(&data); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"message": "Please enter some post data"})
	}

	author := c.Locals("user")

	post.Title = data.Title
	post.Content = data.Content
	post.Writer = author.(models.User).UserName

	if result := initializers.Db.Create(&post); result.Error != nil {
		return c.Status(fiber.StatusBadRequest).JSON(result.Error.Error())
	}

	return c.Status(fiber.StatusOK).JSON(post)
}

func GetPosts(c *fiber.Ctx) error {
	var posts []models.Post
	title, _ := url.QueryUnescape(c.Query("title"))
	author, _ := url.QueryUnescape(c.Query("author"))

	query := initializers.Db
	if title != "" {
		query = query.Where("title LIKE ?", "%"+title+"%")
	}

	if author != "" {
		query = query.Where("writer like ?", "%"+author+"%")
	}

	if err := query.Find(&posts).Error; err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"message": "Did not find books"})
	}

	if len(posts) == 0 {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"message": "Did not find books"})
	}

	return c.Status(fiber.StatusOK).JSON(posts)
}

func DeletePost(c *fiber.Ctx) error {
	var post models.Post
	id, err := c.ParamsInt("id")

	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"message": "Please enter a valid id"})
	}

	initializers.Db.Find(&post, "id = ?", id)

	if post.ID == 0 {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"message": "book does not exist"})
	}

	if err := initializers.Db.Unscoped().Delete(&post).Error; err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"message": "error deleating data"})
	}

	return c.Status(fiber.StatusOK).JSON(fiber.Map{"messsage": "deleted succesfully"})
}
